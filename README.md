#ga-extend

UMD Module for shallow extending objects; heavily inspired by Chris Ferdinandi's [Ditching jQuery: extend](http://gomakethings.com/ditching-jquery/#extend)

At time of writing this it seems ECMAScript 6's [Object.assign](https://github.com/sindresorhus/object-assign) offers the same functionality.

##Installation

```bash
npm install ga-extend --save
```

##Usage

```js
var extend = require('ga-extend');
extend(targetObject, object1, object2);
```

##Develop with Gulp

Use [Git Flow](https://github.com/nvie/gitflow), always work in `develop` branch.

- Install development dependencies: `npm install`
- Run `gulp watch`
- Work in `src/`

Builds are now in `dist/`