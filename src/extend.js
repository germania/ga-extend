(function (root, factory) {
    if (typeof define === "function" && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else if (typeof exports === "object") {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.extend = factory();
    }
}(this, function () {


    function extend () {

        // The merged object
        var result = {};

        // The merge
        var merge = function (obj) {
            Object.keys(obj).forEach(function(key) { result[key] = obj[key]; });
        };

        // Loop over each given argument
        for (var i = 0; i < arguments.length; i++) {
            var obj = arguments[i];
            if (typeof obj !== "undefined") {
                merge(obj);
            }
        }

        // The merged target
        return result;

    }

    return extend;
}));


